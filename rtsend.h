/**
 * @file rtsend.c
 * @brief RTSend module.
 * UART output module: 1 start bit, 1 stop bit, 8 data bit, even parity.
 *
 * Frame format: 
 *
 * STARTBYTE | BYTE1 BYTE2 ... BYTEn | TAILBYTE.
 * 
 * Frame length @a n can be defined staticaly (macro @ref RTSEND_STATIC_FRAME_LENGTH) or dynamicaly (function @ref rtsend_set_frame_length).
 *
 * Frame body is double buffered. Content of @b acumulator_buffer can be freely changed at any time beetween @ref rtsend_get_accum_lock (return pointer to buffer) and @ref rtsend_accum_release.
 * After @ref rtsend_accum_release changes move to @a send_buffer. Transmit strategy depends on module mode of operation @ref RTSendMode.
 *
 * @ref rtsend_poll function performs actual data transmition, it has to be called periodically or by interrupts (see macro @ref RTSEND_ISR).
 */

#ifndef  RTSEND_H
#define  RTSEND_H

#include "config.h"

#include "bool.h"
#include "rtcommon.h"

/**
 * @define RTSEND_FRAME_LENGTH
 * @brief Frame length (accumulator length is equal to frame length).
 * If this macro is not defined frame length is equal to zero and can be changed dynamicaly.
 * Use @ref rtsend_init to set frame length.
 */

/**
 * @define RTSEND_ISR
 * @brief Declare ISR mode.
 * Define this macro to enable ISR mode. @ref rtsend_poll is called by interrupt service routine.
 */


/**
 * @brief RTSend module states
 */
enum RTSendState {
	RTSEND_STOPPED = 0, //< tansmission is disabled,
	RTSEND_ACTIVE = 1, //< accumulator content has alredy been transferred, 
	RTSEND_WAITING = 2, //< waiting for accumulator update
};

/**
 * @brief RTSend modes of operation
 * In continous mode frames are transmited without any pause. In wait accum mode accumulator content is transmitted after each @ref rtsend_accum_release call 
 * only one time.
 */
enum RTSendMode {
	RTSEND_STOP, ///< disable transmitter immediatly,
	RTSEND_SEND_CONT , ///< transmit accumulator content continously.
	RTSEND_SEND_WAIT_ACCUM, ///< transmit accumulator content if only it was not transfered yet.
	RTSEND_SEND_STOP, ///< disable transitter after current transmittion is finished.
};

typedef uint8_t rtsend_mode_t; //< RTSend mode of operation
typedef uint8_t rtsend_state_t; //< RTSend state

/**
 * @fn size_t rtsend_frame_length(void);
 * @brief Return frame length.
 */
#if !defined(RTSEND_FRAME_LENGTH) 
size_t rtsend_frame_length(void);
#else
#	define rtsend_frame_length() RTSEND_FRAME_LENGTH
#endif

/** 
 * @brief Get RTSend state.
 * @return current state @ref RTSendState
 */
rtsend_state_t rtsend_get_state(void);

/**
 * @brief Change RTSend mode of operation.
 * @param cmd New mode @ref RTSendMode
 */
void rtsend_set_mode(rtsend_mode_t _mode);

/**
 * @brief Return nonzero if the accumulator has been coppied to internal buffer.
 */
BOOL rtsend_is_accum_yanked(void);

/**
 * @brief Get pointer to the accumulator and disable send buffer update.
 * Prevent send buffer update from accumulator until @ref rtsend_accum_release is called.
 * @return pointer to accumulator 
 */
void * rtsend_get_accum_lock(void);

/**
 * @brief Enable send buffer update from accumulator.
 */
void rtsend_accum_release(void);

/**
 * @brief Worker function. Should be called periodicaly if ISR mode is not declared (@ref RTSEND_ISR).
 * @return nonzero on success
 */
#if !defined(RTSEND_ISR)
void rtsend_poll(void);
#endif

/**
 * @brief Initialize transiver and set frame length.
 * Do nothing if state is not equal to @a RTSEND_STOPPED.
 * UART mode: 1 start bit, 1 stop bit, 8 data bit, even parity.
 * If macro @ref RTSEND_FRAME_LENGTH is defined, \p frame_length parameter is ignored.
 * @param length frame length in bytes.
 * @return zero if memory allocation failed or device is not ready.
 */ 
BOOL rtsend_init(size_t length);

/**
 * @brief Deinitialize transiver and free allocated memory.
 * If macro @ref RTSEND_FRAME_LENGTH is defined, memory is not freed.
 * @return zero if device is not ready.
 */ 
BOOL rtsend_deinit(void);

#endif  /*RTSEND_H*/
