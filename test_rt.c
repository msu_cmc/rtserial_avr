#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include <stdio.h>
#include <string.h>

#include "uart0.h"
#include "rtsend.h"
#include "rtrecv.h"

// Timer initialization
void  init_delay() {
	// CTC --- Clear Timer on Compare match. Timer TCNT1 goes up and is cleared if TCNT1 == OCR1A. 
	// CTC, TOP in OCR1A, clkT = clk/1024 = 16000000/1024 = 15625; 
	// Interrupts: Compare match on TCNT1 == OCR1A, 
	// Overflow: TCN1 == MAX = 0xFFFF.  
	TCCR1A = 0;
	TCCR1B = _BV(WGM12) | _BV(CS10) |  _BV(CS12);

	OCR1A = 15625; // 1 second delay
	//Enable Output Compare A Interrupt
	TIMSK1 |= _BV(OCIE1A);
}


// Compare match handler
ISR(TIMER1_COMPA_vect, ISR_NOBLOCK) {
	char * recv_accum;
	char * send_accum;
	size_t size;

	recv_accum = rtrecv_get_accum_lock();
	send_accum = rtsend_get_accum_lock();

	size = rtsend_frame_length();

	memcpy(send_accum, recv_accum, size);
	putc('>', stdout);
	putc(*recv_accum, stdout);

	rtrecv_accum_release();
	rtsend_accum_release();

	printf("R:%d T:%d\n", rtrecv_get_state(), rtsend_get_state());
}

int main() {
	init_uart0();
	stdout = &uart0;
	init_delay();

	puts_P(PSTR("start"));
	TCNT1 = 0; // Reset timer.

	rtsend_init(11);
	rtrecv_init(11);

	rtrecv_set_mode(RTRECV_RECV_CONT);
	rtsend_set_mode(RTSEND_SEND_WAIT_ACCUM);

	sei();

	for(;;) { 
#if !defined(RTSEND_ISR)
		rtsend_poll();
#endif
#if !defined(RTRECV_ISR)
		rtrecv_poll();
#endif
	}
	return 0;
}
