# $Id: Makefile,v 1.1 2006/09/26 18:18:27 rschaten Exp $
TARGET=test_rt

AVRDUDE = avrdude -p atmega1280 -P /dev/avr-programmer -c avrispv2

COMPILE = /usr/bin/avr-gcc --std=c99 -Wall -Os -I. -mmcu=atmega1280 -DF_CPU=16000000UL #-DDEBUG_LEVEL=2
# NEVER compile the final product with debugging! Any debug output will
# distort timing so that the specs can't be met.

OBJECTS = test_rt.o rtsend.o rtrecv.o uart0.o
# Note that we link usbdrv.o first! This is required for correct alignment of
# driver-internal global variables!


# symbolic targets:
all:	$(TARGET).hex

.c.o:
	$(COMPILE) -c $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

.c.s:
	$(COMPILE) -S $< -o $@

program:	all
	$(AVRDUDE) -U flash:w:$(TARGET).hex

clean:
	rm -f $(TARGET).hex $(TARGET).lst $(TARGET).obj $(TARGET).cof $(TARGET).list $(TARGET).map $(TARGET).eep.hex $(TARGET).bin *.o usbdrv/*.o $(TARGET).s usbdrv/oddebug.s usbdrv/usbdrv.s

# file targets:
$(TARGET).bin:	$(OBJECTS)
	$(COMPILE) -o $(TARGET).bin $(OBJECTS)

$(TARGET).hex:	$(TARGET).bin
	rm -f $(TARGET).hex $(TARGET).eep.hex
	/usr/bin/avr-objcopy -j .text -j .data -O ihex $(TARGET).bin $(TARGET).hex

disasm:	$(TARGET).bin
	/usr/bin/avr-objdump -d $(TARGET).bin

cpp:
	$(COMPILE) -E $(TARGET).c
